#ifndef BOARD_H_
#define BOARD_H_

#include <iostream>
#include <vector>
#include <utility>
#include <random>

namespace y_mcts {
  enum class Marker { red, blue, none };

  class Board {
    private:
      const int size_;
      std::vector<int> column_sizes_;
      Marker current_player_;
      std::vector<Marker> board_;
      Marker const& operator () (int col, int row) const;
      Marker& operator () (int col, int row);
    public:
      Board(int size);
      void play(int col, int row);
      Marker winner();
      friend std::ostream& operator << (std::ostream &s, const Board &b);
      std::vector<std::pair<int,int> > open_positions();
      void random_playout(std::default_random_engine rng);
  };

}


#endif // BOARD_H_
