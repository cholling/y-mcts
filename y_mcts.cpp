#define DOCTEST_CONFIG_IMPLEMENT
#include "test/doctest.h"

#include <iostream>
#include "board.hpp"

#define PROJECT_NAME "y-mcts"

using namespace y_mcts;
using std::cout;
using std::cin;
using std::string;
using std::endl;
using std::getline;
using std::stoi;

int main(int argc, char **argv) {
    if (argc <= 1) {
        cout << "Requires a size argument." << endl;
        return 1;
    }
    std::random_device rd;
    auto size = stoi(argv[1]);
    auto num_playouts = stoi(argv[2]);
    auto b = Board(size);
    cout << b << endl;
    while(b.winner() == Marker::none) {
        cout << ":> ";
        string parsed;
        getline(cin, parsed, ' ');
        auto col = stoi(parsed);
        getline(cin, parsed, '\n');
        auto row = stoi(parsed);
        // currently only allows human player first
        try {
            b.play(col, row);
            // add code to skip playouts if the human player wins
            std::pair<int, int> best_position {0, 0};
            double best_score = 0.0;
            // need to add code to skip playouts if there's only one open position
            for (auto position : b.open_positions()) {
                int red_wins = 0;
                int blue_wins = 0;
                for (auto x=0; x<num_playouts; x++) {
                    auto temp_board = b;
                    temp_board.play(position.first, position.second);
                    auto rng = std::default_random_engine { rd() };
                    temp_board.random_playout(rng);
                    Marker w = temp_board.winner();
                    if (w == Marker::red) {
                        red_wins++;
                    } else blue_wins++;
                }
                auto new_score = blue_wins / (double) (blue_wins + red_wins);
                if (new_score >= best_score) {
                    best_position = position;
                    best_score = new_score;
                }
            }
            b.play(best_position.first, best_position.second);
            cout << "Playing best position: " << best_position.first << "," << best_position.second << "  " << (100.0 * best_score) << "% wins" << endl;
            cout << b << endl;
        }
        catch (std::out_of_range) {
            cout << "Position does not exist" << endl;
        }
        catch (std::invalid_argument) {
            cout << "Position has already been played" << endl;
        }
    }
    cout << "The winner is " << (b.winner() == Marker::red ? "red!" : "blue!") << endl;

    return 0;
}
