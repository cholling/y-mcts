#include "board.hpp"
#include <string>
#include <cmath>
#include <sstream>
#include "test/doctest.h"
#include <algorithm>

namespace y_mcts {

  // TODO: add an "undo" feature
  Board::Board(int size) : size_(size) {
    for (auto x=size_; x > 0; x--) {
      column_sizes_.push_back(x);
      for (auto y=0; y < x; y++) board_.push_back(Marker::none);
    }
    current_player_ = Marker::red;
  }

  std::ostream& operator << (std::ostream &s, const Board &b)
  {
    int rows = (b.size_ * 2) - 1;
    for (int i=0; i<rows; i++) {
      int x = 0;
      if (i % 2 == 1) {
        x++;
        s << "  ";
      }
      int y = i / 2;
      int yy = y;
      int xx = x;
      int min_yy = 0;
      if (i > rows / 2) min_yy = (i - (rows / 2));
      while ((yy >= 0) && (yy >= min_yy)) {
        std::string repr;
        switch (b(xx, yy)) {
        case Marker::red :
          repr = 'X';
          break;
        case Marker::blue:
          repr = 'O';
          break;
        default :
          repr = '.';
        }

        s << repr;
        if ((yy > 0) && (yy > min_yy)) s << "   ";
        xx += 2;
        yy--;
      }
      s << std::endl;
    }
    return s;
  }

  Marker const& Board::operator () (int col, int row) const {
    auto index = 0;
    for(auto i=0; i < col; i++) { index += column_sizes_[i]; }
    index += row;
    return board_[index];
  }

  Marker& Board::operator () (int col, int row) {
    auto index = 0;
    for(auto i=0; i < col; i++) { index += column_sizes_[i]; }
    index += row;
    return board_[index];
  }

  void Board::play(int col, int row) {
    if (col >= this->size_ || row >= column_sizes_[col]) {
      throw std::out_of_range("Index out of range");
    }
    if ((*this)(col, row) != Marker::none) {
      throw std::invalid_argument("Position is not empty");
    }
    (*this)(col, row) = current_player_;
    if (current_player_ == Marker::red)
      current_player_ = Marker::blue;
    else current_player_ = Marker::red;
  }

  Marker Board::winner() {
    auto b = *this;
    for (auto subtractor = 1; subtractor < this->size_; subtractor++) {
      for (auto col = 0; col < (this->size_ - subtractor); col++) {
        for (auto row = 0; row < this->column_sizes_[col] - subtractor; row++) {
          if ((b(col,row+1) == b(col,row)) || (b(col+1,row) == b(col,row))) {
            // do nothing, since b(col,row) is already the desired value
          } else if (b(col,row+1) == b(col+1,row)) {
            b(col,row) = b(col,row+1);
          } else {
            b(col,row) = Marker::none;
          }
        }
      }
    }


    return b(0,0);
  }

  std::vector<std::pair<int,int> > Board::open_positions() {
    std::vector<std::pair<int,int> > output;
    for (int col = 0; col < this->size_; col++) {
      for (int row = 0; row < this->column_sizes_[col]; row++) {
        if ( (*this)(col, row) == Marker::none ) {
          output.push_back(std::make_pair(col, row));
        }
      }
    }
    return output;
  }

  void Board::random_playout(std::default_random_engine rng) {
    auto open_positions = this->open_positions();
    std::shuffle(std::begin(open_positions), std::end(open_positions), rng);
    for (auto p : open_positions) {
      this->play(p.first, p.second);
    }
  }

  TEST_CASE("String representation") {
    std::string expected;
    std::stringstream out;

    SUBCASE("Size 5") {
      auto b = Board(5);
      expected = ".\n"
        "  .\n"
        ".   .\n"
        "  .   .\n"
        ".   .   .\n"
        "  .   .\n"
        ".   .\n"
        "  .\n"
        ".\n";
      out << b;
    }

    SUBCASE("Size 6") {
      auto b = Board(6);
      expected = ".\n"
        "  .\n"
        ".   .\n"
        "  .   .\n"
        ".   .   .\n"
        "  .   .   .\n"
        ".   .   .\n"
        "  .   .\n"
        ".   .\n"
        "  .\n"
        ".\n";
      out << b;
    }

    CHECK(out.str() == expected);
  }

  TEST_CASE("Bounds checking") {
    SUBCASE("Size 5") {
      auto b = Board(5);
      CHECK_THROWS_AS(b.play(0, 5), const std::out_of_range&);
      CHECK_THROWS_AS(b.play(1, 4), const std::out_of_range&);
      CHECK_THROWS_AS(b.play(4, 1), const std::out_of_range&);
    };

    SUBCASE("Size 7") {
      auto b = Board(7);
      CHECK_THROWS_AS(b.play(0, 7), const std::out_of_range&);
      CHECK_THROWS_AS(b.play(1, 6), const std::out_of_range&);
      CHECK_THROWS_AS(b.play(6, 1), const std::out_of_range&);
    };
  }

  TEST_CASE("Play") {
    std::string expected;
    std::stringstream out;

    SUBCASE("Size 5") {
      auto b = Board(5);
      expected = ".\n"
        "  .\n"
        ".   .\n"
        "  .   .\n"
        "X   .   .\n"
        "  .   .\n"
        ".   O\n"
        "  .\n"
        ".\n";
      b.play(0, 2);
      b.play(2, 2);
      out << b;
    }

    SUBCASE("Size 6") {
      auto b = Board(6);
      expected = ".\n"
        "  .\n"
        ".   .\n"
        "  .   .\n"
        "X   .   .\n"
        "  .   .   .\n"
        ".   O   .\n"
        "  .   .\n"
        ".   .\n"
        "  .\n"
        ".\n";
      b.play(0, 2);
      b.play(2, 2);
      out << b;
    }

    CHECK(out.str() == expected);
  }

  TEST_CASE("Copy semantics") {
    auto b1 = Board(5);
    auto b2 = b1;
    std::string expected_b1, expected_b2;
    std::stringstream out_b1, out_b2;
    expected_b1 = ".\n"
      "  .\n"
      ".   .\n"
      "  .   .\n"
      "X   .   .\n"
      "  .   .\n"
      ".   .\n"
      "  .\n"
      ".\n";
    expected_b2 = ".\n"
      "  .\n"
      ".   .\n"
      "  .   .\n"
      ".   .   .\n"
      "  .   .\n"
      ".   X\n"
      "  .\n"
      ".\n";
    b1.play(0, 2);
    b2.play(2, 2);
    out_b1 << b1;
    out_b2 << b2;
    CHECK(out_b1.str() == expected_b1);
    CHECK(out_b2.str() == expected_b2);
  }


  TEST_CASE("Winner") {
    std::string expected;
    std::stringstream out;
    SUBCASE("Size 2") {
      auto b = Board(2);
      CHECK(b.winner() == Marker::none);
      b.play(1, 0);
      CHECK(b.winner() == Marker::none);
      b.play(0, 0);
      CHECK(b.winner() == Marker::none);
      b.play(0, 1);
      CHECK(b.winner() == Marker::red);
      expected = "O\n"
        "  X\n"
        "X\n";
      out << b;
    }

    SUBCASE("Size 3") {
      auto b = Board(3);
      CHECK(b.winner() == Marker::none);
      b.play(0,0);
      CHECK(b.winner() == Marker::none);
      b.play(0,1);
      CHECK(b.winner() == Marker::none);
      b.play(0,2);
      CHECK(b.winner() == Marker::none);
      b.play(1,1);
      CHECK(b.winner() == Marker::none);
      b.play(1,0);
      CHECK(b.winner() == Marker::none);
      b.play(2,0);
      CHECK(b.winner() == Marker::blue);
      expected = "X\n"
        "  X\n"
        "O   O\n"
        "  O\n"
        "X\n";
      out << b;
    }

    CHECK(out.str() == expected);
  }

  TEST_CASE("Open positions") {
    std::vector<std::pair<int,int> > expected {
      std::make_pair(0,0),
      std::make_pair(0,1),
      std::make_pair(1,0),
    };
    auto b = Board(2);
    CHECK(b.open_positions() == expected);
    b.play(0,0);
    expected.erase(expected.begin());
    CHECK(b.open_positions() == expected);
  }

  TEST_CASE("Random playout") {
    std::random_device rd;
    auto rng = std::default_random_engine { rd() };
    SUBCASE("Size 2") {
      auto b = Board(2);
      b.random_playout(rng);
      CHECK(b.winner() != Marker::none);
    }
    SUBCASE("Size 3") {
      auto b = Board(3);
      b.random_playout(rng);
      CHECK(b.winner() != Marker::none);
    }
    SUBCASE("Size 3 with one move already played") {
      auto b = Board(3);
      b.play(0,1);
      b.random_playout(rng);
      CHECK(b.winner() != Marker::none);
    }
  }
}
